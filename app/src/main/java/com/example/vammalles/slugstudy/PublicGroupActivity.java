package com.example.vammalles.slugstudy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.vammalles.slugstudy.models.Group;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;

public class PublicGroupActivity extends AppCompatActivity implements Serializable {
    String key;
    Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_group);
        Bundle bundle = getIntent().getExtras();
        Group group = (Group) bundle.getSerializable("group");
        Button joinButton = findViewById(R.id.joinButton);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        joinButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                group.addMember(user.getUid());
                Intent intent = new Intent(PublicGroupActivity.this, GroupActivity.class);
                intent.putExtra("group", group);
                startActivity(intent);
            }
        });


        this.key = getIntent().getStringExtra("group.key");
        TextView title = findViewById(R.id.grp_title);
        TextView desc = findViewById(R.id.grp_desc);
        TextView day = findViewById(R.id.grp_day);
        TextView loc = findViewById(R.id.grp_loc);

        String d = "Description: \n" + group.description;

        title.setText(group.name);
        desc.setText(d);
        if (group.time != null) {
            String tm = "When: \n" + group.time;
            day.setText(tm);
        }
        if (group.location != null) {
            String lc = "Where: \n" + group.location;
            loc.setText(lc);
        }



    }

    public void leaveGroup(Group g) {
        g.admins = null;
        Intent intent = new Intent(PublicGroupActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

}