package com.example.vammalles.slugstudy.models;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@IgnoreExtraProperties
public class Group implements Serializable {
    public String key;
    public String name;
    public String description;
    public boolean listed;
    public String time;
    public String location;
    public List<String> admins;
    public List<String> members;

    public Group() {
        // Default constructor required for calls to DataSnapshot.getValue(Group.class)
    }

    public Group(String key, String name, String description, Boolean listed) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.listed = listed;
        this.members = new ArrayList<String>();
        this.admins = new ArrayList<String>();
    }

    public Group(String key, String name, String description, Boolean listed, String adminUser) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.listed = listed;
        this.members = new ArrayList<String>();
        this.admins = new ArrayList<String>();
        this.admins.add(adminUser);
    }

    public Group(String key, String name, String description, Boolean listed, String adminUser, String time, String location) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.listed = listed;
        this.time = time;
        this.location = location;
        this.members = new ArrayList<String>();
        this.admins = new ArrayList<String>();
        this.admins.add(adminUser);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void addMember(String uid) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        this.members.add(uid);
        if (this.key != null) {
            mDatabase.child("groups").child(this.key).child("members").setValue(this.members);
        }
    }

    public void removeMember(String uid) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        this.members.remove(uid);
        if (this.members.isEmpty()) {
            this.members.add("dummy");
        }
        if (this.key != null) {
            mDatabase.child("groups").child(this.key).child("members").setValue(this.members);
        }
    }
}