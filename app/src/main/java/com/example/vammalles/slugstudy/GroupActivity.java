package com.example.vammalles.slugstudy;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.vammalles.slugstudy.models.Group;
import com.example.vammalles.slugstudy.models.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class GroupActivity extends AppCompatActivity implements Serializable {
    String key;
    Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Bundle bundle = getIntent().getExtras();
        DatabaseReference group_db = FirebaseDatabase.getInstance().getReference().child("groups");
        Group group = (Group) bundle.getSerializable("group");

        Button inviteButton = findViewById(R.id.inviteButton);
        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GroupActivity.this, InvitePage.class);
                intent.putExtra("group", group);
                startActivity(intent);
            }
        });

        Button leaveButton = findViewById(R.id.leaveButton);
        leaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                group.removeMember(uid);
                finish();
            }
        });
        Button homeButton = findViewById(R.id.home_button);
        Button msgButton = findViewById(R.id.msgButton);
        msgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(GroupActivity.this, ChatActivity.class);
                intent.putExtra("room", group.key);
                startActivity(intent);
            }
        });
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GroupActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });


        this.key = getIntent().getStringExtra("group.key");
        TextView title = findViewById(R.id.grp_title);
        TextView desc = findViewById(R.id.grp_desc);
        TextView day = findViewById(R.id.grp_day);
        TextView loc = findViewById(R.id.grp_loc);

        String d = "Description: \n" + group.description;

        title.setText(group.name);
        desc.setText(d);
        if (group.time != null) {
            String tm = "When: \n" + group.time;
            day.setText(tm);
        }
        if (group.location != null) {
            String lc = "Where: \n" + group.location;
            loc.setText(lc);
        }



    }

    public void leaveGroup(Group g) {
        g.admins = null;
        Intent intent = new Intent(GroupActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

}