package com.example.vammalles.slugstudy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.vammalles.slugstudy.models.Group;

import org.w3c.dom.Text;

public class InvitePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_page);
        Bundle bundle = getIntent().getExtras();
        Group group = (Group) bundle.getSerializable("group");
        TextView groupid = findViewById(R.id.groupid);
        groupid.setText(group.key);

        Button doneButton = findViewById(R.id.leaveInvite);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
