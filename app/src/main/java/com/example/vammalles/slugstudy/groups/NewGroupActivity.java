package com.example.vammalles.slugstudy.groups;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.vammalles.slugstudy.R;
import com.example.vammalles.slugstudy.Validation;
import com.example.vammalles.slugstudy.models.Group;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NewGroupActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);

        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        findViewById(R.id.submitButton).setOnClickListener((View view) -> {
            EditText nameField = findViewById(R.id.nameInput);
            EditText descriptionField = findViewById(R.id.descriptionInput);
            EditText whenField = findViewById(R.id.whenInput);
            EditText whereField = findViewById(R.id.whereInput);
            String name = nameField.getText().toString().trim();
            String description = descriptionField.getText().toString().trim();
            String when = whenField.getText().toString().trim();
            String where = whereField.getText().toString().trim();

            if (name.equals("")) {
                nameField.setError(getString(R.string.name_invalid));
                return;
            }

            if (description.equals("")) {
                descriptionField.setError(getString(R.string.description_invalid));
                return;
            }

            if (when.equals("")) {
                descriptionField.setError("Time is required");
                return;
            }

            if (where.equals("")) {
                descriptionField.setError("Location is required");
                return;
            }

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            assert user != null;
            String key = this.mDatabase.child("groups").push().getKey();
            Group group = new Group(key, name, description,
                    ((RadioButton) findViewById(R.id.publicRadioButton)).isChecked(),
                    user.getUid(), when, where);
            group.addMember(user.getUid());
            if (group.key != null) {
                this.mDatabase.child("groups").child(group.key).setValue(group);
            }

            finish();
        });
    }

}
