package com.example.vammalles.slugstudy;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vammalles.slugstudy.groups.NewGroupActivity;
import com.example.vammalles.slugstudy.models.Group;
import com.example.vammalles.slugstudy.models.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Messages.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Messages#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Messages extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private DatabaseReference msgDb;
    private String recent_message;

    public LinearLayout message_layout;

    public Messages() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Messages.
     */
    // TODO: Rename and change types and number of parameters
    public static Messages newInstance(String param1, String param2) {
        Messages fragment = new Messages();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        message_layout = view.findViewById(R.id.message_layout);


        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        Query myGroupsQuery = mDatabase.child("groups");
        myGroupsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Group group = snapshot.getValue(Group.class);
                    assert group != null;
                    if (group.members.contains(user.getUid())) {
                        getRecentMessage(group);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public CardView getMessageTile(Group group, Message message) {
        CardView tile = new CardView(getActivity());
        tile.setCardElevation(15);
        tile.setContentPadding(100, 50, 100, 100);
        tile.setRadius(15);
        tile.setMinimumHeight(300);
        tile.setClickable(true);
        tile.setOutlineAmbientShadowColor(getResources().getColor(R.color.colorPrimaryDark));


        // Create the layout inside of the CardView
        LinearLayout tile_layout = new LinearLayout(getActivity());
        tile_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        tile_layout.setOrientation(LinearLayout.VERTICAL);
        tile.addView(tile_layout);

        // Set title on newly added tile
        TextView tile_title = new TextView(getActivity());
        tile_title.setText(group.name);
        tile_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        tile_title.setPadding(30, 10, 0, 0);
        tile.setContentPadding(30, 30, 30, 0);
        tile_layout.addView(tile_title);

        // Get the most recent message from the group and display it on the message tile
        TextView tile_last_message = new TextView(getActivity());
        tile_last_message.setText(message.getMessageText());
        tile_last_message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tile_last_message.setPadding(30, 10, 0, 0);
        tile_layout.addView(tile_last_message);


        // Color
        tile.setBackgroundColor(getResources().getColor(R.color.card_blue));

        return tile;
    }

    public void getRecentMessage(Group group) {
        msgDb = FirebaseDatabase.getInstance().getReference();
        Query msg_query = msgDb.child("messages").child(group.key);

        msg_query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Message msg = new Message();
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    msg = messageSnapshot.getValue(Message.class);
                }
                if (msg.getMessageText() == null) {
                    msg = new Message(group.key, "No messages yet", group.name);
                }
                CardView tile = getMessageTile(group, msg);
                message_layout.addView(tile);
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) tile.getLayoutParams();
                layoutParams.setMargins(0, 10, 0, 0);
                tile.requestLayout();
                    // Setup onClick listener to redirect to the GroupActivity
                    tile.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent intent = new Intent(getActivity(), ChatActivity.class);

                            // Need to change this to group id instead of group name for practical use
                            intent.putExtra("room", group.key);
                            startActivity(intent);
                        }
                    });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });

    }

}
