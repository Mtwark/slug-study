package com.example.vammalles.slugstudy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vammalles.slugstudy.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateAccountActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    private String name;
    private String email;
    private String password;
    public User usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        getSupportActionBar().hide();

        mAuth = FirebaseAuth.getInstance();

        findViewById(R.id.buttonCreateAccount).setOnClickListener((View view) -> {
            EditText emailField = findViewById(R.id.editTextEmail);
            EditText passwordField = findViewById(R.id.editTextPassword);
            EditText passwordVerificationField = findViewById(R.id.editTextReEnterPassword);
            EditText usernameField = findViewById(R.id.editTextUsername);

            if (!Validation.isValidEmail(emailField.getText())) {
                emailField.setError(getString(R.string.email_invalid));
                return;
            }

            if (passwordField.getText().toString().equals("")) {
                passwordField.setError(getString(R.string.password_invalid));
                return;
            }

            if (!passwordField.getText().toString().equals(passwordVerificationField.getText().toString())) {
                passwordVerificationField.setError(getString(R.string.password_verify_invalid));
                return;
            }

            if (usernameField.getText().toString().equals("")) {
                usernameField.setError(getString(R.string.username_invalid));
                return;
            }

            this.name = usernameField.getText().toString().trim();
            this.email = emailField.getText().toString().trim();
            this.password = passwordField.getText().toString();

            mAuth.createUserWithEmailAndPassword(this.email, this.password)
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            Log.d("AUTH", "createUserWithEmail:success");

                            // create new user object: we have to do this in order to store the
                            // user's name
                            FirebaseUser user = mAuth.getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(this.name)
                                    .build();
                            if (user != null) {
                                user.updateProfile(profileUpdates);
                                usr = new User(user.getUid(), this.name, this.email);
                                DatabaseReference db = FirebaseDatabase.getInstance().getReference();
                                String dbKey = db.child("users").push().getKey();
                                db.child("users").child(dbKey).setValue(usr);

                            }

                            Intent intent = new Intent(CreateAccountActivity.this, DashboardActivity.class);
                            startActivity(intent);
                        } else {
                            Log.w("AUTH", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(CreateAccountActivity.this, "Failed to create user account.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

        });
    }
}
