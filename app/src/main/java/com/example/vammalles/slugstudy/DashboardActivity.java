package com.example.vammalles.slugstudy;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.Menu;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vammalles.slugstudy.models.Group;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.io.Serializable;

public class DashboardActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener,
        Serializable,
        Home.OnFragmentInteractionListener,
        Search.OnFragmentInteractionListener,
        Messages.OnFragmentInteractionListener


{



    public boolean reloadNedeed = true;

    private void fetch() {
        Query query = FirebaseDatabase.getInstance().getReference().child("groups");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        setTitle("Dashboard");

        navView.setOnNavigationItemSelectedListener(this);
        loadFragment(new Home());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.top_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id==R.id.Edit_Profile){
            Toast.makeText(this, "Edit Profile", Toast.LENGTH_SHORT).show();
        }
        if(id==R.id.Logout){
            Toast.makeText(this, "You have successfully logged out!", Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(DashboardActivity.this, Login.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        boolean skip = false;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new Home();
                break;
            case R.id.navigation_search:
                fragment = new Search();
                break;
            case R.id.navigation_messages:
                //Intent tempIntent = new Intent(DashboardActivity.this, ChatActivity.class);
                //tempIntent.putExtra("room_slug", "test");
                //tartActivity(tempIntent);
                //skip = false;
                fragment = new Messages();
                break;
        }
        //if (skip) {
        //    return true;
        //}
        return loadFragment(fragment);
    }

    public CardView getTile(Group group) {
        // Create tile and setup its parameters
        CardView tile = new CardView(this);
        tile.setCardElevation(15);
        tile.setContentPadding(100, 50, 100, 100);
        tile.setRadius(15);
        tile.setMinimumHeight(500);
        tile.setClickable(true);
        //tile.setOutlineAmbientShadowColor(getResources().getColor(R.color.colorPrimaryDark));


        // Create the layout inside of the CardView
        LinearLayout tile_layout = new LinearLayout(this);
        tile_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        tile_layout.setOrientation(LinearLayout.VERTICAL);
        tile.addView(tile_layout);

        // Set title on newly added tile
        TextView tile_title = new TextView(this);
        tile_title.setText(group.name);
        tile_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        tile_title.setPadding(30, 10, 0, 0);
        tile.setContentPadding(30, 30, 30, 0);
        tile_layout.addView(tile_title);

        // Set description for tile
        TextView tile_desc = new TextView(this);
        tile_desc.setPadding(30, 100, 0, 0);
        String desc = "Description: \n" + group.description;
        tile_desc.setText(desc);
        tile_desc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tile_layout.addView(tile_desc);


        // We might allow these params to be optional, so check if they're null first
        if (group.time != null) {
            TextView tile_time = new TextView(this);
            tile_time.setPadding(30, 20, 0, 0);
            String time = "When: \n" + group.time;
            tile_time.setText(time);
            tile_time.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tile_layout.addView(tile_time);
        }

        if (group.time != null) {
            TextView tile_location = new TextView(this);
            tile_location.setPadding(30, 20, 0, 50);
            String loc = "Where: \n" + group.location;
            tile_location.setText(loc);
            tile_location.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tile_layout.addView(tile_location);
        }

        // Color
        tile.setBackgroundColor(getResources().getColor(R.color.card_blue));

        return tile;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //@Override
    //public void onListFragmentInteraction(DummyContent.GroupItem item) {

    //}
}
